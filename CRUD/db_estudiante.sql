create database db_estudiante;

use db_estudiante;

create table estudiante(
id_estudiante int primary key auto_increment,
nombre varchar(50),
apellido varchar(50),
direccion varchar(50),
carrera varchar(50)
); 